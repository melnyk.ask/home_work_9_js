
// 1. Опишіть, як можна створити новий HTML тег на сторінці.
// Створити новий тег на сторінці можна за допомогою методів
// document.createElement() та document.createTextNode(). Перший
// з методів створює елемент з заданим тегом, другий - текстовий
// вузел з заданим текстом.
// Після цього необхідно додати створений елемент на сторінку. Для цього
// інують відповідні методи: append, prepend, before, after, replaceWith.
// Також можна змінити innerHTML, додавши необхідні теги, що приведе
// до вставки в елемент (тобто створення) іншого тегу(тегів).
// Крім того, існують методи insertAdjacenHTML, insertAdjacentText,
// insertAdjacentElement, які дозволяють вставити html-рядок (з тегами),
// просто рядок (текст) і елемент відповідно.

// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML
// і опишіть можливі варіанти цього параметра.
// Перший параметр функції визначає місце, куди буде виконуватись вставка
// html-рядка у визначений елемент:
// element.insertAdjacentHTML(param, 'html-рядок з тегами');
// Перший параметр (param) може приймати значення:
// "beforebegin" – вставляє html-рядок безпосередньо перед element,
// "afterbegin" – вставляє html-рядок в початок element,
// "beforeend" – вставляє html-рядок в кінець element,
// "afterend" – вставляє html-рядок безпосередньо після element.

// 3. Як можна видалити елемент зі сторінки?
// Для цього інує метод node.remove().

function build(arr = [], element = document.body) {

let seconds_remain = document.createElement('div');

function recursion(arr, element, param) {
    
let ul = document.createElement('ul');
ul.classList.add("list");
element.append(ul);
    let check = param;
    if (check === 1) { 
        let li2 = document.createElement('li');
        li2.append(ul);
        element.append(li2);
    }
    
arr.map(
    (elem) => { 
        if (Array.isArray(elem) !== true) {
            let li = document.createElement('li');
            li.textContent = String(elem);
            ul.append(li);
            //ul.innerHTML += `<li>${String(elem)}</li>`;
        }
        else { 
            recursion(elem, ul, 1);
        }
    }   
    ).join("");
}    

recursion(arr, element);
    
element.append(seconds_remain);

seconds_remain.textContent = `3 seconds remain..`;
let i = 3;
    
let setinterval = setInterval(
    () => {
    i--;
    seconds_remain.textContent = `${i} seconds remain..`;    
    },
    1000
);    

setTimeout(
    () => {
        document.querySelectorAll('.list').forEach(
            (elem) => { 
                elem.remove();
            }
        );
        seconds_remain.remove();
        clearInterval(setinterval);
    },
    3000
);

}

let arr = [[1, 2, "Stepan", ["AAA", "BBB"]], "hello", "world", "Kiev", "Kharkiv", ["SSS", 2, [22, 44]], "Odessa", "Lviv"];

build(arr, document.querySelector('.test'));
//build(arr, document.body);
//build([123, 2, "ghghg"], document.querySelector('main'));


